$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 1000
    });
    $('#contacto').on('show.bs.modal', function(e){
        console.log('el modal se esta mostrando');
        $('#contactobtn').removeClass('btn-primary');
        $('#contactobtn').addClass('btn-secondary');
        $('#contactobtn').prop('disabled',true);
    });
    $('#contacto').on('hidden.bs.modal', function(e){

        $('#contactobtn').removeClass('btn-secondary');
        $('#contactobtn').addClass('btn-primary');
        $('#contactobtn').prop('disabled',false);

    });
    
});

$('body').css('padding-top', $('.navbar').outerHeight() + 'px')

// detect scroll top or down
if ($('.smart-scroll').length > 0) { // check if element exists
    var last_scroll_top = 0;
    $(window).on('scroll', function() {
        scroll_top = $(this).scrollTop();
        if(scroll_top < last_scroll_top) {
            $('.smart-scroll').removeClass('scrolled-down').addClass('scrolled-up');
        }
        else {
            $('.smart-scroll').removeClass('scrolled-up').addClass('scrolled-down');
        }
        last_scroll_top = scroll_top;
    });
}